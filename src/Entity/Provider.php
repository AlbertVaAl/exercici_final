<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class Provider
 * @ORM\Entity
 * @ORM\Table(name="provider")
 */

class Provider
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="Nom", type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\Column (name="Correu", type="string", nullable=false)
     */
    private $mail;

    /**
     * @ORM\Column (name="Telefon", type="integer", nullable=false)
     */
    private $phone;

    /**
     * @ORM\Column (name="Tipus", type="string", nullable=false)
     */
    private $ProviderType;

    /**
     * @ORM\Column (name="Estat", type="string", nullable=false)
     */
    private $Active;



    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getMail()
    {
        return $this->mail;
    }
    public function getPhone()
    {
        return $this->phone;
    }
    public function getProviderType()
    {
        return $this->ProviderType;
    }
    public function getActive()
    {
        return $this->Active;
    }


    public function setId($id): void
    {
        $this->id = $id;
    }
    public function setName($name): void
    {
        $this->name = $name;
    }
    public function setMail($mail): void
    {
        $this->mail = $mail;
    }
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }
    public function setProviderType($ProviderType): void
    {
        $this->ProviderType = $ProviderType;
    }
    public function setActive($Active): void
    {
        $this->Active = $Active;
    }
}