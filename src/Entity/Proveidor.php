<?php

namespace App\Entity;

use App\Repository\ProveidorRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProveidorRepository::class)
 * @ORM\Table (name="Proveidor")
 */
class Proveidor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Correu;

    /**
     * @ORM\Column(type="integer")
     */
    private $Telefon;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Tipus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Estat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getCorreu(): ?string
    {
        return $this->Correu;
    }

    public function setCorreu(string $Correu): self
    {
        $this->Correu = $Correu;

        return $this;
    }

    public function getTelefon(): ?int
    {
        return $this->Telefon;
    }

    public function setTelefon(int $Telefon): self
    {
        $this->Telefon = $Telefon;

        return $this;
    }

    public function getTipus(): ?string
    {
        return $this->Tipus;
    }

    public function setTipus(string $Tipus): self
    {
        $this->Tipus = $Tipus;

        return $this;
    }

    public function getEstat(): ?string
    {
        return $this->Estat;
    }

    public function setEstat(string $Estat): self
    {
        $this->Estat = $Estat;

        return $this;
    }
}
