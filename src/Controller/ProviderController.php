<?php

namespace App\Controller;


use App\Entity\Proveidor;
use App\Entity\Provider;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProviderController extends AbstractController
{
    /**
     *  @Route("/index", name="index");
    */
      private $providers;

      protected $container;

      private $entityM;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this -> providers = [];
        return $this -> render('index.html.twig');
    }

    public function test(Request $request){
        return $this -> render('index.html.twig');

    }

    /**
     * @Route ("/proveidor/guardar", name="guardar_proveidor", methods={"POST"})
     *
     */
    public function crearProvider(Request $request): Response
    {

        $name = $request->request->get('Nom');
        $mail = $request->request->get('Correu');
        $phone = $request->request->get('Telefon');
        $ProviderType = $request->request->get('Tipus');
        $Active = $request->request->get('Estat');


        $provider = new Proveidor();

        $provider->setNom($name);
        $provider->setCorreu($mail);
        $provider->setTelefon($phone);
        $provider->setTipus($ProviderType);
        $provider->setEstat($Active);

        $entityM = $this->getDoctrine()->getManager();
        $entityM->persist($provider);
        $entityM->flush();

        return $this->redirectToRoute('mostrar_proveidors');
    }
    /**
     * @Route ("/proveidor/{id}/editar}", name="editar_proveidor")
     *
     */
    public function editarProvider(Request $request, $id): Response
    {
        $entityM = $this->getDoctrine()->getManager();
        $provider = $entityM->getRepository(Proveidor::class)->find($id);

        if (!$provider) {
            throw $this->createNotFoundException("No s'ha trobat el proveidor amb l'ID: ", $id);
        }
        $name = $request->request->get('Nom');
        $mail = $request->request->get('Correu');
        $phone = $request->request->get('Telefon');
        $ProviderType = $request->request->get('Tipus');
        $Active = $request->request->get('Estat');

        $provider->setNom($name);
        $provider->setCorreu($mail);
        $provider->setTelefon($phone);
        $provider->setTipus($ProviderType);
        $provider->setEstat($Active);

        $entityM->flush();

        return $this->redirectToRoute('mostrar_proveidors');

    }

    /**
     * @Route ("/proveidor/eliminar/{id}", name="eliminar_proveidor")
     *
     */
    public function eliminarProvider($id)
    {
        $entityM = $this->getDoctrine()->getManager();
        $provider = $entityM->getRepository(Proveidor::class)->find($id);

        if (!$provider){
            throw $this->createNotFoundException("No s'ha trobat el proveidor amb l'ID: ", $id);
        }
        $entityM->remove($provider);
        $entityM->flush();

        return$this->redirectToRoute('mostrar_proveidors');
    }

    /**
     * @Route ("/proveidor/mostrar", name="mostrar_proveidors")
     *
     */

    public function veureProveidors(){
        $providers = $this->getDoctrine()->getRepository(Proveidor::class)->findAll();
        return $this->render('taula.html.twig', [
            'providers' => $providers,
            ]);
    }

    /**
     * @Route ("/proveidor/{id}", name="veure_proveidor")
     *
     */
    public function veureProveidor($id)
    {
        $provider = $this->getDoctrine()->getRepository(Proveidor::class)->find($id);

        return $this->render('editar.html.twig',[
            'provider' => $provider,
        ]);
    }
}